#!/bin/bash

source "$(realpath "${BASH_SOURCE%/*}/libs/env")"
source "${SCRIPT_PATH}/libs/functions"
source "${SCRIPT_PATH}/libs/sonar_functions"
source "${SCRIPT_PATH}/libs/maven_functions"

cd "$SCRIPT_PATH" || exit 1
cd .. || exit 1

print_random_banner

print_execution_environment_information

set_maven_memory
print_maven_execution_environment

echo

# Sonar in Docker
#################

set_sonar_environment_variables

check_if_docker_is_running

check_if_sonar_container_exists

run_sonar

wait_for_sonar_endpoint

echo && change_sonar_admin_password && echo

# Run analysis
##############

./mvnw -P sonar sonar:sonar

open -a "Google Chrome" "http://localhost:9000/projects"
