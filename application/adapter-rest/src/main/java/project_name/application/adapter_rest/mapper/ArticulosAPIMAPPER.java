package project_name.application.adapter_rest.mapper;

import project_name.application.adapter_rest.model.ArticuloREST;
import project_name.domain.model.ArticuloDOMAIN;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ArticulosAPIMAPPER {

    ArticulosAPIMAPPER INSTANCE = Mappers.getMapper(ArticulosAPIMAPPER.class);

    ArticuloDOMAIN aDominio(ArticuloREST articuloREST);

    @Mapping(source = "ean13", target = "ean")
    ArticuloREST aREST(ArticuloDOMAIN articulo);

}
