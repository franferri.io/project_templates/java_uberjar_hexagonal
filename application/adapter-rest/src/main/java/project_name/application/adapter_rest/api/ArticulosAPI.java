package project_name.application.adapter_rest.api;

import project_name.application.adapter_rest.model.ArticuloREST;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/v1")
public interface ArticulosAPI {

    @GetMapping("/articulos/{name}")
    ArticuloREST getArticulo(@PathVariable String name);

}
