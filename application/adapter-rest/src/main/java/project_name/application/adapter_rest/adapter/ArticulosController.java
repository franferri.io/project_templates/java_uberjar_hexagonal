package project_name.application.adapter_rest.adapter;

import project_name.application.adapter_rest.api.ArticulosAPI;
import project_name.application.adapter_rest.mapper.ArticulosAPIMAPPER;
import project_name.application.adapter_rest.model.ArticuloREST;
import project_name.domain.model.ArticuloDOMAIN;
import project_name.domain.ports.application.RestPort;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ArticulosController implements ArticulosAPI {

    private final RestPort articulosService;
    private final ArticulosAPIMAPPER mapper = ArticulosAPIMAPPER.INSTANCE;

    public ArticuloREST getArticulo(String name) {

        System.out.println("HEXAGONAL");

        System.out.println("Llamada recibida en el ADAPTADOR DE APLICACIÓN REST al método getArticulo, con valor: " + name);

        ArticuloDOMAIN articulo = articulosService.getArticulo(name);

        System.out.println("El el ADAPTADOR DE APLICACIÓN REST devuelve los datos recibidos del DOMINIO que consume");

        return mapper.aREST(articulo);

    }

}



