# Example API

> https://franferri.io/templates

* Clone to get with: `git clone --depth 1 --recurse-submodules --shallow-submodules <repository-url>`
* Compile and run with: `./bin/dev_run.sh`
* Compile and run FAST with: `./bin/dev_run.sh --fast`
* Run tests: `./bin/tests_run.sh`
* Coverage: `./bin/report_conventions.sh`
* Conventions: `./bin/report_coverage.sh`

## Jargon for this template scripts

### Custom per user

**Local** is your local machine, you may be macOS but others may be Linux or Windows.
**Dev** is your local machine connecting to remote services like databases providers, buckets, etc.
**Tests** will run locally unit tests and integration tests (this dont includes end to end tests)

### Environments

**Staging** is a remote environment
**Production** is a remote environment
