package project_name.domain.ports.application;

import project_name.domain.model.ArticuloDOMAIN;

public interface RestPort {

    ArticuloDOMAIN getArticulo(String name);

}
