package project_name.domain.services;

import project_name.domain.model.ArticuloDOMAIN;
import project_name.domain.ports.application.RestPort;
import project_name.domain.ports.infrastructure.DatabasePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/** Database, me traiga un artículo, cuando se lo pida por rest **/
@Service
@RequiredArgsConstructor
public class ArticulosService implements RestPort {

    private final DatabasePort  database;

    public ArticuloDOMAIN getArticulo(String name){

        System.out.println("Llamada recibida al DOMINIO al método getArticulo, con valor: " + name);

        ArticuloDOMAIN articulo = database.getArticuloByName(name);

        System.out.println("El DOMINIO devuelve los valores recibidos del ADAPTADOR DE INFRAESTRUCTURA DE BASE DE DATOS que consume");

        return articulo;
    }



}
