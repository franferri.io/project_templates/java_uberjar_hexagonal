package project_name.infrastructure.adapter_database.mapper;


import project_name.domain.model.ArticuloDOMAIN;
import project_name.infrastructure.adapter_database.model.ArticuloMO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ArticulosMAPPER {

    ArticulosMAPPER INSTANCE = Mappers.getMapper(ArticulosMAPPER.class);


    @Mapping(source = "eanMerca", target = "ean13")
    ArticuloDOMAIN aDominio(ArticuloMO articuloMO);

    ArticuloMO aMO(ArticuloDOMAIN articulo);

}
